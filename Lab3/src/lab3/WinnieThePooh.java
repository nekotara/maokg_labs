package lab3;


import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import javafx.util.Duration;

public class WinnieThePooh extends Application {

    public static void main(String args[]) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        Scene scene = new Scene(root, 800, 600);

        Ellipse ear1 = new Ellipse(184, 16, 12, 10);
        ear1.setStroke(Color.BLACK);
        ear1.setStrokeWidth(1);
        ear1.setFill(Color.rgb(254, 196, 62));
        root.getChildren().add(ear1);

        Ellipse ear2 = new Ellipse(233, 18, 12, 20);
        ear2.setStroke(Color.BLACK);
        ear2.setStrokeWidth(1);
        ear2.setFill(Color.rgb(254, 196, 62));
        ear2.setRotate(20);
        root.getChildren().add(ear2);
        //head
        var head1 = new MoveTo(165, 130);
        var head2 = new QuadCurveTo(132, 106, 132, 70);
        var head3 = new QuadCurveTo(157, 63, 144, 47);
        var head4 = new QuadCurveTo(140, 37, 155, 35);
        var head5 = new QuadCurveTo(171, 34, 187, 20);
        var head6 = new QuadCurveTo(240, 15, 249, 43);
        var head7 = new QuadCurveTo(261, 64, 252, 102);
        var head7_1 = new QuadCurveTo(217, 135, 165, 130);

        var head = new Path(head1, head2, head3, head4, head5, head6, head7, head7_1);
        head.setStrokeWidth(1);
        head.setStroke(Color.BLACK);
        head.setFill(Color.rgb(254, 196, 62));
        root.getChildren().add(head);

        var mouth1 = new MoveTo(187, 73);
        var mouth2 = new CubicCurveTo(170, 124, 100, 52, 166, 57);
        var mouth3 = new MoveTo(161, 57);
        var mouth4 = new LineTo(163, 45);
        var mouth = new Path(mouth1, mouth2, mouth3, mouth4);
        mouth.setStroke(Color.BLACK);
        mouth.setFill(Color.rgb(254, 196, 62));
        root.getChildren().add(mouth);

        Ellipse nose = new Ellipse(144, 62, 9, 7);
        nose.setFill(Color.BLACK);
        root.getChildren().add(nose);

        Ellipse eye1 = new Ellipse(156, 52, 1, 2);
        eye1.setFill(Color.BLACK);
        root.getChildren().add(eye1);

        Ellipse eye2 = new Ellipse(186, 48, 1, 2);
        eye2.setFill(Color.BLACK);
        root.getChildren().add(eye2);

        var eyebrow1 = new MoveTo(148, 47);
        var eyebrow2 = new QuadCurveTo(147, 39, 155, 38);
        var eyebrowLeft = new Path(eyebrow1, eyebrow2);
        eyebrowLeft.setStrokeWidth(2.5);
        eyebrowLeft.setStroke(Color.BLACK);
        root.getChildren().add(eyebrowLeft);

        var eyebrow3 = new MoveTo(181, 29);
        var eyebrow4 = new QuadCurveTo(191, 25, 197, 33);
        var eyebrowRight = new Path(eyebrow3, eyebrow4);
        eyebrowRight.setStrokeWidth(2.5);
        eyebrowRight.setStroke(Color.BLACK);
        root.getChildren().add(eyebrowRight);

        var eyeW = new MoveTo(182, 55);
        var eyeW1 = new QuadCurveTo(185, 49, 193, 50);
        var eyeW2 = new MoveTo(154, 92);
        var eyeW3 = new QuadCurveTo(154, 100, 164, 103);
        var eyeW4 = new Path(eyeW, eyeW1, eyeW2, eyeW3);
        eyeW4.setStrokeWidth(1);
        eyeW4.setStroke(Color.BLACK);
        root.getChildren().add(eyeW4);

        var leg8 = new MoveTo(84, 224);
        var leg9 = new LineTo(70, 176);
        var leg10 = new LineTo(31, 187);
        var leg11 = new QuadCurveTo(27, 157, 2, 172);
        var leg12 = new LineTo(1, 211);
        var leg13 = new QuadCurveTo(2, 227, 25, 223);
        var leg14 = new LineTo(84, 224);

        var leg15 = new Path(leg8, leg9, leg10,leg11, leg12, leg13, leg14);
        leg15.setStrokeWidth(1);
        leg15.setStroke(Color.BLACK);
        leg15.setFill(Color.rgb(254, 196, 62));
        root.getChildren().add(leg15);

        Ellipse legBack2 = new Ellipse(5, 196, 3,22);
        legBack2.setStrokeWidth(1);
        legBack2.setStroke(Color.BLACK);
        legBack2.setFill(Color.rgb(238, 162, 0));
        root.getChildren().add(legBack2);

        var body = new MoveTo(210, 214);
        var body1 = new QuadCurveTo(181, 140, 135, 129);
        var body2 = new QuadCurveTo(19, 148, 99, 244);
        var body3 = new QuadCurveTo(171, 264, 210, 214);
        var body4 = new Path(body, body1, body2, body3);
        body4.setStrokeWidth(1);
        body4.setStroke(Color.BLACK);
        body4.setFill(Color.rgb(254, 196, 62));
        root.getChildren().add(body4);

        var shirt = new MoveTo(136, 101);
        var shirt1 = new QuadCurveTo(133, 112, 118, 115);
        var shirt2 = new LineTo(121, 133);
        var shirt3 = new LineTo(136, 130);
        var shirt4 = new QuadCurveTo(133, 112, 148, 111);
        var shirt5 = new LineTo(136, 101);

        var shirt6 = new MoveTo(148, 111);
        var shirt7 = new QuadCurveTo(166, 136, 190, 121);
        var shirt8 = new QuadCurveTo(224, 98, 253, 103);
        var shirt9 = new LineTo(250, 95);
        var shirt10 = new QuadCurveTo(279, 99, 260, 115);
        var shirt11 = new QuadCurveTo(273, 122, 258, 139);
        var shirt12 = new LineTo(221, 225);
        var shirt13 = new LineTo(204, 223);
        var shirt14 = new LineTo(210, 215);
        var shirt15 = new CubicCurveTo(187, 190, 161, 133, 126, 130);
        var shirt16 = new QuadCurveTo(132, 117, 148, 111);

        var shirt17 = new Path(shirt, shirt1, shirt2, shirt3, shirt4, shirt5);
        var shirt18 = new Path(shirt6, shirt7, shirt8, shirt9, shirt10, shirt11, shirt12, shirt13, shirt14, shirt15, shirt16);
        shirt17.setStrokeWidth(1);
        shirt17.setStroke(Color.BLACK);
        shirt17.setFill(Color.rgb(238, 8, 46));
        root.getChildren().add(shirt17);

        shirt18.setStrokeWidth(1);
        shirt18.setStroke(Color.BLACK);
        shirt18.setFill(Color.rgb(238, 8, 46));
        root.getChildren().add(shirt18);


        //arm
        var head8 = new MoveTo(259, 169);
        var head8_1 = new LineTo(267, 220);
        var head9 = new QuadCurveTo(295, 231, 261, 244);
        var head10 = new QuadCurveTo(239, 244, 235, 225);
        var head11 = new LineTo(222, 180);
        var head12 = new LineTo(259, 169);
        var head13 = new Path(head8, head8_1,head9, head10, head11, head12);
        head13.setStrokeWidth(1);
        head13.setStroke(Color.BLACK);
        head13.setFill(Color.rgb(254, 196, 62));
        root.getChildren().add(head13);

        Polygon armLeft = new Polygon(120,124,121,133,110,134);
        armLeft.setStrokeWidth(1);
        armLeft.setStroke(Color.BLACK);
        armLeft.setFill(Color.rgb(254, 196, 62));
        root.getChildren().add(armLeft);

        var sleeve = new MoveTo(219, 178);
        var sleeve1 = new CubicCurveTo(215, 119, 257, 111, 262, 168);
        var sleeve3 = new QuadCurveTo(243, 182, 219, 178);
        var sleeve4 = new Path(sleeve, sleeve1, sleeve3);
        sleeve4.setStrokeWidth(1);
        sleeve4.setStroke(Color.BLACK);
        sleeve4.setFill(Color.rgb(238, 8, 46));
        root.getChildren().add(sleeve4);

        var leg = new MoveTo(163, 243);
        var leg1 = new QuadCurveTo(162, 215, 115, 227);
        var leg2 = new LineTo(85, 256);
        var leg3 = new QuadCurveTo(71, 233, 54, 256);
        var leg4 = new LineTo(56, 288);
        var leg5 = new QuadCurveTo(57, 304, 87, 293);
        var leg6 = new LineTo(163, 243);

        var leg7 = new Path(leg, leg1, leg2,leg3, leg4, leg5, leg6);
        leg7.setStrokeWidth(1);
        leg7.setStroke(Color.BLACK);
        leg7.setFill(Color.rgb(254, 196, 62));
        root.getChildren().add(leg7);

        Ellipse legBack = new Ellipse(62, 270, 7.5,22);
        legBack.setStrokeWidth(1);
        legBack.setStroke(Color.BLACK);
        legBack.setFill(Color.rgb(238, 162, 0));
        root.getChildren().add(legBack);



        // Animation
        int cycleCount = 2;
        int time = 2000;

        TranslateTransition translateTransition0 = new TranslateTransition(Duration.millis(time), root);
        translateTransition0.setFromY(150);
        translateTransition0.setToY(50);
        translateTransition0.setAutoReverse(true);

        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(time), root);
        scaleTransition.setToX(2);
        scaleTransition.setToY(2);
        scaleTransition.setAutoReverse(true);

        RotateTransition rotateTransition = new RotateTransition(Duration.millis(time), root);
        rotateTransition.setByAngle(180f);
        rotateTransition.setCycleCount(cycleCount);
        rotateTransition.setAutoReverse(true);

        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(time), root);
        translateTransition.setFromX(150);
        translateTransition.setToX(50);
        translateTransition.setCycleCount(cycleCount + 1);
        translateTransition.setAutoReverse(true);

        TranslateTransition translateTransition2 = new TranslateTransition(Duration.millis(time), root);
        translateTransition2.setFromX(50);
        translateTransition2.setToX(150);
        translateTransition2.setCycleCount(cycleCount + 1);
        translateTransition2.setAutoReverse(true);

        TranslateTransition translateTransition3 = new TranslateTransition(Duration.millis(time), root);
        translateTransition3.setFromY(50);
        translateTransition3.setToY(150);
        translateTransition3.setCycleCount(cycleCount + 1);
        translateTransition3.setAutoReverse(true);

        ScaleTransition scaleTransition2 = new ScaleTransition(Duration.millis(time), root);
        scaleTransition2.setToX(0.1);
        scaleTransition2.setToY(0.1);
        scaleTransition2.setCycleCount(cycleCount);
        scaleTransition2.setAutoReverse(true);

        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(
                translateTransition,
                translateTransition2,
                translateTransition3,
                rotateTransition,
                scaleTransition,
                scaleTransition2
        );
        parallelTransition.setCycleCount(Timeline.INDEFINITE);
        parallelTransition.play();
//        // End of animation

        primaryStage.setResizable(false);
        primaryStage.setTitle("Lab 3");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
