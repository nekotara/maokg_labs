package lab3;


import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Fish extends Application {

    public static void main(String args[]) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        Scene scene = new Scene(root, 1200, 600);

        //fish scale 1
        var scale1 = new MoveTo(130, 36);
        var scale2 = new LineTo(143, 6);
        var scale3 = new QuadCurveTo(195, 8, 219, 49);
        var scale4 = new QuadCurveTo(168, 23, 130, 36);
        var scaleUp = new Path(scale1, scale2, scale3, scale4);
        scaleUp.setStrokeWidth(1);
        scaleUp.setStroke(Color.BLACK);
        scaleUp.setFill(Color.rgb(239, 99, 32));
        root.getChildren().add(scaleUp);

        //mouth
        var mouth1 = new MoveTo(53, 114);
        var mouth2 = new QuadCurveTo(33, 113, 41, 130);
        var mouth3 = new QuadCurveTo(33, 146, 50, 150);
        var mouth4 = new LineTo(64, 155);
        var mouth5 = new QuadCurveTo(72, 130, 53, 114);
        var mouth = new Path(mouth1, mouth2, mouth3, mouth4, mouth5);
        mouth.setStrokeWidth(1);
        mouth.setStroke(Color.BLACK);
        mouth.setFill(Color.rgb(227, 191, 1));
        root.getChildren().add(mouth);

        //body
        var body = new Ellipse(150, 116, 100, 90);
        body.setFill(Color.rgb(227, 191, 1));
        body.setStroke(Color.BLACK);
        body.setStrokeWidth(1);

        root.getChildren().add(body);

        var dotted = new MoveTo(113, 40);
        var dotted1 = new QuadCurveTo(158, 120, 114, 205);
        var dotted3 = new Path(dotted, dotted1);
        dotted3.getStrokeDashArray().addAll(2d, 10d);
        dotted3.setStrokeWidth(2);
        dotted3.setStroke(Color.WHITE);
        root.getChildren().add(dotted3);

        var shell1_1 = new MoveTo(128, 53);
        var shell1_2 = new QuadCurveTo(170, 57, 139, 84);
        var shell1_3 = new LineTo(128, 53);

        var shell2_1 = new MoveTo(140, 93);
        var shell2_2 = new QuadCurveTo(173, 103, 144, 124);
        var shell2_3 = new LineTo(140, 93);

        var shell3_1 = new MoveTo(142, 131);
        var shell3_2 = new QuadCurveTo(175, 145, 143, 160);
        var shell3_3 = new LineTo(142, 131);

        var shellPurple = new Path(shell1_1, shell1_2, shell1_3, shell2_1, shell2_2, shell2_3, shell3_1, shell3_2, shell3_3);
        shellPurple.setStrokeWidth(0);
        shellPurple.setFill(Color.rgb(127, 116, 176));

        root.getChildren().add(shellPurple);

        var dotted_shell1_1 = new MoveTo(131, 57);
        var dotted_shell1_2 = new QuadCurveTo(158, 57, 141, 81);

        var dotted_shell2_1 = new MoveTo(142, 97);
        var dotted_shell2_2 = new QuadCurveTo(166, 104, 142, 121);

        var dotted_shell3_1 = new MoveTo(146, 134);
        var dotted_shell3_2 = new QuadCurveTo(167, 144, 146, 157);

        var dotted_shellPurple = new Path(dotted_shell1_1, dotted_shell1_2, dotted_shell2_1, dotted_shell2_2, dotted_shell3_1, dotted_shell3_2);

        dotted_shellPurple.getStrokeDashArray().addAll(1d, 5d);
        dotted_shellPurple.setStrokeWidth(2);
        dotted_shellPurple.setStroke(Color.WHITE);

        root.getChildren().add(dotted_shellPurple);

        var dot1 = new Ellipse(138, 66, 3, 3);
        dot1.setFill(Color.WHITE);

        var dot2 = new Ellipse(146, 107, 3, 3);
        dot2.setFill(Color.WHITE);

        var dot3 = new Ellipse(148, 145, 3, 3);
        dot3.setFill(Color.WHITE);

        root.getChildren().add(dot1);
        root.getChildren().add(dot2);
        root.getChildren().add(dot3);

        var shell4_1 = new MoveTo(154, 85);
        var shell4_2 = new QuadCurveTo(170, 77, 170, 63);
        var shell4_3 = new QuadCurveTo(200, 79, 175, 99);
        var shell4_4 = new QuadCurveTo(169, 86, 154, 85);

        var shell5_1 = new MoveTo(154, 126);
        var shell5_2 = new QuadCurveTo(170, 119, 170, 105);
        var shell5_3 = new QuadCurveTo(200, 121, 175, 141);
        var shell5_4 = new QuadCurveTo(175, 129, 154, 126);

        var shell6_1 = new MoveTo(154, 170);
        var shell6_2 = new QuadCurveTo(170, 163, 170, 149);
        var shell6_3 = new QuadCurveTo(200, 165, 175, 185);
        var shell6_4 = new QuadCurveTo(175, 173, 154, 170);

        var shellGreen = new Path(shell4_1, shell4_2, shell4_3, shell4_4, shell5_1, shell5_2, shell5_3, shell5_4, shell6_1, shell6_2, shell6_3, shell6_4);
        shellGreen.setStrokeWidth(0);
        shellGreen.setFill(Color.rgb(64, 176, 107));

        root.getChildren().add(shellGreen);

        var dotted_shell4_1 = new MoveTo(173, 67);
        var dotted_shell4_2 = new QuadCurveTo(195, 82, 174, 94);

        var dotted_shell5_1 = new MoveTo(173, 109);
        var dotted_shell5_2 = new QuadCurveTo(195, 124, 174, 136);


        var dotted_shell6_1 = new MoveTo(173, 153);
        var dotted_shell6_2 = new QuadCurveTo(195, 168, 174, 180);

        var dotted_shellGreen = new Path(dotted_shell4_1, dotted_shell4_2, dotted_shell5_1, dotted_shell5_2, dotted_shell6_1, dotted_shell6_2);

        dotted_shellGreen.getStrokeDashArray().addAll(1d, 5d);
        dotted_shellGreen.setStrokeWidth(2);
        dotted_shellGreen.setStroke(Color.WHITE);

        root.getChildren().add(dotted_shellGreen);

        var dot4 = new Ellipse(174, 81, 3, 3);
        dot4.setFill(Color.WHITE);

        var dot5 = new Ellipse(174, 123, 3, 3);
        dot5.setFill(Color.WHITE);

        var dot6 = new Ellipse(174, 167, 3, 3);
        dot6.setFill(Color.WHITE);

        root.getChildren().add(dot4);
        root.getChildren().add(dot5);
        root.getChildren().add(dot6);

        var shell7_1 = new MoveTo(188, 102);
        var shell7_2 = new QuadCurveTo(202, 97, 207, 87);
        var shell7_3 = new QuadCurveTo(227, 104, 204, 122);
        var shell7_4 = new QuadCurveTo(201, 109, 188, 102);

        var shell8_1 = new MoveTo(189, 149);
        var shell8_2 = new QuadCurveTo(202, 144, 205, 134);
        var shell8_3 = new QuadCurveTo(228, 153, 203, 168);
        var shell8_4 = new QuadCurveTo(200, 155, 189, 149);

        var shellPink = new Path(shell7_1, shell7_2, shell7_3, shell7_4, shell8_1, shell8_2, shell8_3, shell8_4);
        shellPink.setStrokeWidth(0);
        shellPink.setFill(Color.rgb(245, 98, 110));

        root.getChildren().add(shellPink);

        var dotted_shell7_1 = new MoveTo(210, 89);
        var dotted_shell7_2 = new QuadCurveTo(225, 103, 206, 118);

        var dotted_shell8_1 = new MoveTo(208, 138);
        var dotted_shell8_2 = new QuadCurveTo(225, 152, 205, 164);

        var dotted_shellPink = new Path(dotted_shell7_1, dotted_shell7_2, dotted_shell8_1, dotted_shell8_2);

        dotted_shellPink.getStrokeDashArray().addAll(1d, 5d);
        dotted_shellPink.setStrokeWidth(2);
        dotted_shellPink.setStroke(Color.WHITE);

        root.getChildren().add(dotted_shellPink);

        var dot7 = new Ellipse(207, 102, 3, 3);
        dot7.setFill(Color.WHITE);

        var dot8 = new Ellipse(205, 151, 3, 3);
        dot8.setFill(Color.WHITE);

        root.getChildren().add(dot7);
        root.getChildren().add(dot8);

        //fish scale 2
        var scale_1 = new MoveTo(150, 178);
        var scale_2 = new LineTo(196, 224);
        var scale_3 = new QuadCurveTo(186, 249, 152, 241);
        var scale_4 = new LineTo(130, 193);
        var scale_5 = new QuadCurveTo(128, 162, 150, 178);
        var scale_Down = new Path(scale_1, scale_2, scale_3, scale_4, scale_5);
        scale_Down.setStrokeWidth(1);
        scale_Down.setStroke(Color.BLACK);
        scale_Down.setFill(Color.rgb(239, 99, 32));
        root.getChildren().add(scale_Down);

        //fish scale 3
        var scale1_3 = new MoveTo(250, 103);
        var scale2_3 = new QuadCurveTo(276, 113, 290, 81);
        var scale3_3 = new QuadCurveTo(303, 107, 282, 139);
        var scale4_3 = new QuadCurveTo(300, 176, 278, 202);
        var scale5_3 = new QuadCurveTo(269, 167, 246, 158);
        var scale6_3 = new QuadCurveTo(237, 151, 246, 141);
        var scale7_3 = new QuadCurveTo(233, 131, 247, 122);
        var scale8_3 = new QuadCurveTo(238, 108, 250, 103);
        var scaleBack = new Path(scale1_3, scale2_3, scale3_3, scale4_3, scale5_3, scale6_3, scale7_3, scale8_3);
        scaleBack.setStrokeWidth(1);
        scaleBack.setStroke(Color.BLACK);
        scaleBack.setFill(Color.rgb(239, 99, 32));
        root.getChildren().add(scaleBack);

        //eye

        var brow1 = new MoveTo(81, 76);
        var brow2 = new QuadCurveTo(90, 71, 98, 76);
        var brow = new Path(brow1, brow2);
        brow.setStrokeWidth(1);
        brow.setStroke(Color.BLACK);
        root.getChildren().add(brow);

        var eyelash1_1 = new MoveTo(83, 88);
        var eyelash1_2 = new QuadCurveTo(78, 86, 82, 83);

        var eyelash2_1 = new MoveTo(88, 87);
        var eyelash2_2 = new QuadCurveTo(84, 84, 86, 82);

        var eyelash3_1 = new MoveTo(91, 87);
        var eyelash3_2 = new QuadCurveTo(89, 83, 92, 82);

        var eyelash4_1 = new MoveTo(97, 89);
        var eyelash4_2 = new QuadCurveTo(95, 84, 99, 83);

        var eyelash = new Path(eyelash1_1, eyelash1_2, eyelash2_1, eyelash2_2, eyelash3_1, eyelash3_2, eyelash4_1, eyelash4_2);
        eyelash.setStrokeWidth(1);
        eyelash.setStroke(Color.BLACK);
        root.getChildren().add(eyelash);

        var eye = new Ellipse(88, 99, 15, 15);
        var eye1 = new Ellipse(88, 104, 10, 10);
        var eye2 = new Ellipse(88, 106, 7, 7);
        var eye3 = new Ellipse(89, 100, 2, 2);

        eye.setStrokeWidth(1);
        eye.setStroke(Color.BLACK);
        eye.setFill(Color.WHITE);

        eye1.setFill(Color.rgb(192, 212, 193));
        eye2.setFill(Color.BLACK);
        eye3.setFill(Color.WHITE);
        root.getChildren().add(eye);
        root.getChildren().add(eye1);
        root.getChildren().add(eye2);
        root.getChildren().add(eye3);


        // Animation
        int cycleCount = 2;
        int time = 2000;

        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(time), root);
        scaleTransition.setToX(2);
        scaleTransition.setToY(2);
        scaleTransition.setAutoReverse(true);

        RotateTransition rotateTransition = new RotateTransition(Duration.millis(time), root);
        rotateTransition.setByAngle(180f);
        rotateTransition.setCycleCount(cycleCount);
        rotateTransition.setAutoReverse(true);

        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(time), root);
        translateTransition.setFromX(150);
        translateTransition.setToX(50);
        translateTransition.setCycleCount(cycleCount + 1);
        translateTransition.setAutoReverse(true);

        TranslateTransition translateTransition2 = new TranslateTransition(Duration.millis(time), root);
        translateTransition2.setFromX(50);
        translateTransition2.setToX(150);
        translateTransition2.setCycleCount(cycleCount + 1);
        translateTransition2.setAutoReverse(true);

        ScaleTransition scaleTransition2 = new ScaleTransition(Duration.millis(time), root);
        scaleTransition2.setToX(0.1);
        scaleTransition2.setToY(0.1);
        scaleTransition2.setCycleCount(cycleCount);
        scaleTransition2.setAutoReverse(true);

        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(
                translateTransition,
                translateTransition2,
                rotateTransition,
                scaleTransition,
                scaleTransition2
        );
        parallelTransition.setCycleCount(Timeline.INDEFINITE);
        parallelTransition.play();
//        // End of animation

        primaryStage.setResizable(false);
        primaryStage.setTitle("Lab 3");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
