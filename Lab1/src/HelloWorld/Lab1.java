package HelloWorld;

import javafx.scene.paint.Color;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.shape.*;

public class Lab1 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Variant 13");

        Group root = new Group();
        Scene scene = new Scene(root, 300, 250);
        //VARIANT 13
        scene.setFill(Color.BLACK);

        Circle circle = new Circle();
        circle.setCenterX(150);
        circle.setCenterY(125);
        circle.setRadius(100);
        circle.setFill(Color.BLUE);
        root.getChildren().add(circle);

        Circle circleRed = new Circle();
        circleRed.setCenterX(150);
        circleRed.setCenterY(125);
        circleRed.setRadius(60);
        circleRed.setFill(Color.RED);
        root.getChildren().add(circleRed);

        Circle circleYel = new Circle();
        circleYel.setCenterX(150);
        circleYel.setCenterY(125);
        circleYel.setRadius(25);
        circleYel.setFill(Color.YELLOW);
        root.getChildren().add(circleYel);

        Circle circleLine = new Circle();
        circleLine.setCenterX(150);
        circleLine.setCenterY(125);
        circleLine.setRadius(80);
        circleLine.setStroke(Color.BLACK);
        circleLine.setStrokeWidth(0.5);
        circleLine.setFill(null);
        root.getChildren().add(circleLine);

        Circle circleLine2 = new Circle();
        circleLine2.setCenterX(150);
        circleLine2.setCenterY(125);
        circleLine2.setRadius(42.5);
        circleLine2.setStroke(Color.BLACK);
        circleLine2.setStrokeWidth(0.5);
        circleLine2.setFill(null);
        root.getChildren().add(circleLine2);

        Circle circleLine3 = new Circle();
        circleLine3.setCenterX(150);
        circleLine3.setCenterY(125);
        circleLine3.setRadius(0.5);
        root.getChildren().add(circleLine3);

        Circle circleLine4 = new Circle();
        circleLine4.setCenterX(150);
        circleLine4.setCenterY(125);
        circleLine4.setRadius(12.5);
        circleLine4.setStroke(Color.BLACK);
        circleLine4.setStrokeWidth(0.5);
        circleLine4.setFill(null);
        root.getChildren().add(circleLine4);


        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
