package Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;

@SuppressWarnings("serial")
class Animation extends JPanel implements ActionListener {
    private static int maxWidth;
    private static int maxHeight;

    double points[][] = {
            { -100, -15 }, { -25, -25 }, { 0, -90 }, { 25, -25 },
            { 100, -15 }, { 50, 25 }, { 60, 100 }, { 0, 50 },
            { -60, 100 }, { -50, 25 }, { -100, -15 }
    };

    Timer timer;

    private double scale = 1;
    private double delta = 0.01;

    private int radius = 200;
    private int radiusExtention = 110;
    private double x = 180;
    private double y = 0;
    private double tx = 6;
    private double ty = 0;
    private double angle = 0;




    public Animation() {
        timer = new Timer(10, this);
        timer.start();
    }

    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHints(rh);

        g2d.setBackground(new Color(251, 219, 255));
        g2d.clearRect(0, 0, maxWidth, maxHeight);

        g2d.translate(maxWidth/2, maxHeight/2);

        BasicStroke bs2 = new BasicStroke(16, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
        g2d.setStroke(bs2);
        g2d.drawRect(
                -(radius + radiusExtention),
                -(radius + radiusExtention),
                (radius + radiusExtention)*2,
                (radius + radiusExtention)*2
        );

        g2d.translate(tx, ty);

        GeneralPath star = new GeneralPath();
        star.moveTo(points[0][0], points[0][1]);
        for (int k = 1; k < points.length; k++)
            star.lineTo(points[k][0], points[k][1]);
        star.closePath();


        GradientPaint gp = new GradientPaint(
                25, 50,
                new Color(172, 38, 255),
                60, 5,
                new Color(249, 255, 128),
                true
        );
        g2d.setPaint(gp);
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) scale));
        g2d.fill(star);

        g2d.setColor(new Color(0, 0, 255));
        g2d.fillOval(-50, -40, 100, 100);

         g2d.setColor(new Color(255, 0, 0));
         g2d.fillOval(-30, -20, 60, 60);

        g2d.setColor(new Color(255, 255, 0));
        g2d.fillOval(-13, -3, 25, 25);

        g2d.setColor(Color.BLACK);
        Shape circle = new Ellipse2D.Double(-40, -30, 80, 80);
        g2d.setStroke(new BasicStroke((float) 0.5));
        g2d.draw(circle);

        Shape circle2 = new Ellipse2D.Double(-21.5, -11.5, 44.5, 44.5);
        g2d.setStroke(new BasicStroke((float) 0.5));
        g2d.draw(circle2);

        Shape circle3 = new Ellipse2D.Double(-6.5, 3.5, 13, 13);
        g2d.setStroke(new BasicStroke((float) 0.5));
        g2d.draw(circle3);

        Shape circle4 = new Ellipse2D.Double(0, 10, 0.5, 0.5);
        g2d.setStroke(new BasicStroke( 1));
        g2d.draw(circle4);

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Lab2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(750, 750);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.add(new Animation());

        frame.setVisible(true);

        Dimension size = frame.getSize();
        Insets insets = frame.getInsets();
        maxWidth = size.width - insets.left - insets.right - 1;
        maxHeight = size.height - insets.top - insets.bottom - 1;
    }

    public void actionPerformed(ActionEvent e) {
        if (scale < 0.01) {
            delta = -delta;
        } else if (scale > 0.99) {
            delta = -delta;
        }

        if(angle > 360) angle = 0;
        tx = x*Math.cos(angle) - y*Math.sin(angle);
        ty = x*Math.sin(angle) + y*Math.cos(angle);
        angle+=0.01;

        scale += delta;

        repaint();
    }
}
